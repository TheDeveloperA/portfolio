import java.awt.EventQueue;
import java.awt.Frame;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;

import javax.imageio.ImageIO;
import javax.swing.Action;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;

/**
 * This program uses the Chuck Norris API:
 * https://api.chucknorris.io/
 * 
 * To display random facts regarding the character himself.
 * 
 * @author Mohammed Azhar Habib
 * @since 24/03/2020
 */
public class Main{

	public static void main(String[] args) {
		
		EventQueue.invokeLater(new Runnable() {
			
			@Override
			public void run() {
				launchGUI();
			}
		});
		
	}
	
	public static void launchGUI() {
		ChuckNorrisFacts facts = new ChuckNorrisFacts();
		String[] result = facts.getFactsandIMG();
		JFrame jFrame = new JFrame("Test");
		jFrame.setSize(400,400);
		jFrame.setLocationRelativeTo(null);
		jFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		JPanel mainPanel = new JPanel();
		mainPanel.setLayout(new GridBagLayout());
		jFrame.add(mainPanel);
		
		GridBagConstraints c = new GridBagConstraints();
		
		c.weighty = 0.5;
		c.gridx = 0;
		c.gridy = 0;
		c.fill = GridBagConstraints.CENTER;
		
		String imgPath = "res/chuck_norris.png";
		ImageIcon icon = new ImageIcon(imgPath);
		JLabel iconLabel = new JLabel();
		iconLabel.setIcon(icon);
		mainPanel.add(iconLabel,c);
		
		c.gridy = 1;
		
		JTextArea factsArea = new JTextArea();
		factsArea.setSize(350, 350);
		factsArea.setText(result[0]);
//		These were used to make the component appear like a JLabel
		factsArea.setBorder(null);
		factsArea.setEditable(false);
//		These two were needed to ensure proper wrapping of long sentences
		factsArea.setLineWrap(true);
		factsArea.setWrapStyleWord(true);
		factsArea.setBackground(null);
		mainPanel.add(factsArea,c);
		
		c.gridy = 2;
		JButton nextButton = new JButton("Refresh");
		
		nextButton.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				
				String fact = facts.getFactsandIMG()[0];
				
				factsArea.setText(fact);
				
			}
		});
		
		mainPanel.add(nextButton,c);
		
		jFrame.setVisible(true);
		
	}
}
