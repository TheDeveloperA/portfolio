import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

import com.google.gson.Gson;
import com.google.gson.JsonObject;

public class ChuckNorrisFacts {
	
	private static final String HOSTNAME = "https://api.chucknorris.io/jokes/random";
	
	public String[] getFactsandIMG() {
//		Querying the API
		
			String result = "";
			
			try {
				URL url = new URL(HOSTNAME);
				HttpURLConnection connection = (HttpURLConnection) url.openConnection();
				connection.setRequestMethod("GET");
//				This was required to prevent an Error 403
				connection.addRequestProperty("User-Agent", "Mozilla/5.0");
				connection.connect();
				
				BufferedReader rd = new BufferedReader(new InputStreamReader(connection.getInputStream()));
			    String line;
			    
			    while ((line = rd.readLine()) != null) {
			    	result += line;
			    }
			} catch (IOException e) {
				e.printStackTrace();
				System.out.println("Could not connect to API");
			}
			
		    // Used to parse the JSON code to something recognised.
		    Gson gson = new Gson(); 
		    JsonObject jsonObject = gson.fromJson(result, JsonObject.class);  
		    String fact = jsonObject.get("value").getAsString();
		    String iconUrl = jsonObject.get("icon_url").getAsString();
		    return  new String[] {fact,iconUrl};
		      		
	}
}
